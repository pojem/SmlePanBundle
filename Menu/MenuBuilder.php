<?php
namespace Smle\PanBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

class MenuBuilder
{
    private $factory;
    
    protected $securityContext;
    
    /**
     * @param FactoryInterface $factory
     * @param SecurityContextInterface $securityContext
     */
    public function __construct(FactoryInterface $factory, SecurityContextInterface $securityContext)
    {
        $this->factory = $factory;
        $this->securityContext = $securityContext;
    }

    public function adminMainMenu(Request $request)
    {
        $menu = $this->factory->createItem('root');

        $menu->addChild('Accueil', array('route' => 'smle_pan_homepage'));
                
        $menu->addChild('Paniers', array('uri' => '#'))
                 ->setAttribute('dropdown', true);
        
        $menu->addChild('Produits', array('uri' => '#'))
                 ->setAttribute('dropdown', true);
        
        $menu->addChild('Tiers', array('uri' => '#'))
                 ->setAttribute('dropdown', true);
        
        $menu['Paniers']->addChild('Commandes', array('route' => 'panierorder'));
        $menu['Paniers']->addChild('Prix', array('route' => 'panier'));
        $menu['Produits']->addChild('Production', array('route' => 'production'));
        $menu['Produits']->addChild('Prix', array('route' => 'product'));
        $menu['Produits']->addChild('Unités', array('route' => 'unit'));
        $menu['Tiers']->addChild('AMAP', array('route' => 'amap'));
        $menu['Tiers']->addChild('Adhérents', array('route' => 'amapadherent'));
        /*
        $menu->addChild('About Me', array(
            'route' => 'page_show',
            'routeParameters' => array('id' => 42)
        ));
        */

        $menu->addChild('Configuration', array('uri' => '#'))
                 ->setAttribute('dropdown', true);
        $menu['Configuration']->addChild('Utilisateurs', array('uri' => $request->getBaseUrl().'/admin/user'));
        $menu['Configuration']->addChild('Profil', array('uri' => $request->getBaseUrl().'/admin/profil'));
                 
        $menu->addChild('Dolibarr', array('uri' => $request->getBaseUrl().'/../../../dolibarr/htdocs', 'target' => '_blank'))
                 ->setAttribute('dropdown', true)
                 ->setLinkAttribute('target', '_blank')
                 ->setAttribute('class', 'external');
                 

        return $menu;
    }

    public function userMainMenu(Request $request)
    {
        //echo $request->getBaseUrl();
        $menu = $this->factory->createItem('root');

        $username = $this->securityContext->getToken()->getUser()->getUsername();
        
        $menu->addChild('Accueil', array(
            'uri' => $request->getBaseUrl().'/smle/'.$username
            ));
        $menu->addChild('Production', array(
            'uri' => $request->getBaseUrl().'/smle/'.$username.'/production'
            ));
        $menu->addChild('Profil', array('uri' => '#'))
                 ->setAttribute('dropdown', true);
        $menu['Profil']->addChild('Profil', array('uri' => $request->getBaseUrl().'/smle/'.$username.'/profil'));

        return $menu;
    }
}

