<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\ProductUnit;
use Smle\PanBundle\Form\ProductUnitType;

/**
 * ProductUnit controller.
 *
 */
class ProductUnitController extends Controller
{
    /**
     * Lists all ProductUnit entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:ProductUnit')->findAll();

        return $this->render('SmlePanBundle:ProductUnit:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a ProductUnit entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:ProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductUnit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:ProductUnit:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new ProductUnit entity.
     *
     */
    public function newAction()
    {
        $request = $this->container->get('request');

        $entity = new ProductUnit();

        if($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();

            $product = $em->getRepository('SmlePanBundle:Product')->find($request->request->get('productId'));
            $entity->setProduct($product);

            $form   = $this->createForm(new \Smle\PanBundle\Form\ProductUnitNewType(), $entity);

            return $this->render('SmlePanBundle:ProductUnit:new.html.twig', array(
                'entity'      => $entity,
                'form'   => $form->createView()
            ));
        }

        return $this->render('SmlePanBundle:ProductUnit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new ProductUnit entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new ProductUnit();
        $form = $this->createForm(new \Smle\PanBundle\Form\ProductUnitNewType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('product_show', array('id' => $entity->getProduct()->getId())));
        }

        return $this->render('SmlePanBundle:ProductUnit:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing ProductUnit entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:ProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductUnit entity.');
        }

        $editForm = $this->createForm(new ProductUnitType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:ProductUnit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing ProductUnit entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:ProductUnit')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ProductUnit entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ProductUnitType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('productunit_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:ProductUnit:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ProductUnit entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:ProductUnit')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ProductUnit entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('productunit'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
