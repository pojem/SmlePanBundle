<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\AmapDeliveryDay;
use Smle\PanBundle\Form\AmapDeliveryDayType;

/**
 * AmapDeliveryDay controller.
 *
 */
class AmapDeliveryDayController extends Controller
{
    /**
     * Lists all AmapDeliveryDay entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->findAll();

        return $this->render('SmlePanBundle:AmapDeliveryDay:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a AmapDeliveryDay entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapDeliveryDay entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:AmapDeliveryDay:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new AmapDeliveryDay entity.
     *
     */
    public function newAction()
    {
        $request = $this->container->get('request');

        $entity = new AmapDeliveryDay();

        if($request->isXmlHttpRequest())
        {
            $em = $this->getDoctrine()->getManager();

            $amapId = $request->request->get('amapId');
            $amap = $em->getRepository('SmlePanBundle:Amap')->find($amapId);
            if (!$amapId) {
                throw $this->createNotFoundException('Unable to find Amap entity.');
            }
            
            $entity->setAmap($amap);

            $form = $this->createForm(new AmapDeliveryDayType(), $entity);

            return $this->render('SmlePanBundle:AmapDeliveryDay:new.html.twig', array(
                'entity'      => $entity,
                'form'   => $form->createView()
            ));
        }
    }

    /**
     * Creates a new AmapDeliveryDay entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new AmapDeliveryDay();
        $form = $this->createForm(new AmapDeliveryDayType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            if($sameEntity = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->findSame($entity->getAmap()->getId(), $entity->getDateStart()->format('Y-m-d'))) {
                $em->remove($sameEntity);
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('amap'));
        }

        return $this->render('SmlePanBundle:AmapDeliveryDay:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AmapDeliveryDay entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapDeliveryDay entity.');
        }

        $editForm = $this->createForm(new AmapDeliveryDayType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:AmapDeliveryDay:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing AmapDeliveryDay entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapDeliveryDay entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new AmapDeliveryDayType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('amapdeliveryday_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:AmapDeliveryDay:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a AmapDeliveryDay entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AmapDeliveryDay entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('amapdeliveryday'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
