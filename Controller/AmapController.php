<?php

namespace Smle\PanBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\Amap;
use Smle\PanBundle\Entity\AmapDeliveryDay;

/**
 * Amap controller.
 *
 */
class AmapController extends Controller
{
    /**
     * Lists all Amap entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:Amap')->findAll();
        
        foreach($entities as &$amap) {
            $today = new \DateTime('today');
            $amapDeliveryDay = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->findLast($amap->getId(), $today);
            if(!$amapDeliveryDay) {
                $amapDeliveryDay = new AmapDeliveryDay;
                $amapDeliveryDay->setDateStart($today);
                $amapDeliveryDay->setDay(-1);
                $amapDeliveryDay->setAmap($amap);
            }
            $amap->addAmapDeliveryDay($amapDeliveryDay);
            
            $contacts = $em->getRepository('SmlePanBundle:AmapAdherent')->findBy(array('amap' => $amap->getId(), 'contact' => true));
            
            $amap->setContacts($contacts != null);
        }
        
        

        return $this->render('SmlePanBundle:Amap:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Amap entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $today = new \DateTime('today');
        
        $dateStart = clone $today;
        $dateStart->modify('-'.($dateStart->format('w') + 34).' Days');
        $dateEnd = clone $dateStart;
        $dateEnd->modify('+63 Days');
        
        $tAmaps = array();

        $conn = $this->container->get('database_connection');
        $sql = "SELECT * FROM view_pan_amap_panier ap WHERE ap.amap_id = ".$id;
        $results = $conn->query($sql);
        
        $tPaniers = array();
        foreach($results as $row) {
            $tPaniers[$row['panier_id']] = $row['panier_name'];
        }
        
        $amap = $em->getRepository('SmlePanBundle:Amap')->find($id);
        
        // Setting temporary default delivery day
        $amapDeliveryDay = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->findLast($amap->getId(), $today);
        if(!$amapDeliveryDay) {
            $amapDeliveryDay = new AmapDeliveryDay;
            $amapDeliveryDay->setDateStart($today);
            $amapDeliveryDay->setDay(-1);
            $amapDeliveryDay->setAmap($amap);
        }
        $amap->addAmapDeliveryDay($amapDeliveryDay);
        
        $contacts = $em->getRepository('SmlePanBundle:AmapAdherent')->findBy(array('amap' => $amap->getId(), 'contact' => true));
        
        $panierOrders = $em->getRepository('SmlePanBundle:PanierOrder')->findAmapAfterDate($dateStart->format('Y-m-d'), $amap->getId());

        // Préparer le tableau vide
        if(count($panierOrders)) {
            foreach($panierOrders as $po) {
                $tAmaps[$po->getDate()->format('W')][$po->getPanier()->getId()] = $po;
            }
        }
/*
echo "<pre>";print_r($tPaniers);echo "</pre>";die();
*/
        
        $tDates = array();
        $dateCur = clone $dateEnd;
        do {
            $tDates[] = clone $dateCur;
            $dateCur->modify('-7 Days');
        }while($dateCur > $dateStart);
        
        return $this->render('SmlePanBundle:Amap:show.html.twig', array(
            'entity' => $amap,
            't_paniers' => $tPaniers,
            't_amaps' => $tAmaps, 
            't_dates' => $tDates,
            'contacts' => $contacts
        ));
    }
}
