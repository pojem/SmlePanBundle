<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\AmapAdherent;
use Smle\PanBundle\Form\AmapAdherentType;
use Smle\PanBundle\Entity\Amap;
use Smle\PanBundle\Entity\PanierAdherent;
use Smle\PanBundle\Form\PanierAdherentType;

/**
 * AmapAdherent controller.
 *
 */
class AmapAdherentController extends Controller
{
    /**
     * Lists all AmapAdherent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        
        $adherents = $em->getRepository('SmlePanBundle:Adherent')->findAll();
        
        foreach($adherents as $adherent)
        {
            if(!$em->getRepository('SmlePanBundle:AmapAdherent')->findByAdherent($adherent->getId()))
            {
                $newAmapAdherent = new AmapAdherent;
                $newAmapAdherent->setContact(false);
                $newAmapAdherent->setAdherent($adherent);
                $em->persist($newAmapAdherent);
            }
            $em->flush();
        }
        $entities = $em->getRepository('SmlePanBundle:AmapAdherent')->findAll();
        
        //Hydrating manually last PanierAdherent if exists
        foreach($entities as &$entity) {
            $panierAdherent = $em->getRepository('SmlePanBundle:PanierAdherent')->findBy(array('amapAdherent' => $entity->getId(), 'date_end' => null));
            if($panierAdherent) {
                $entity->addPanierAdherent($panierAdherent[0]);
            }
//echo $entity->getAmap()->getName();        
        }
        $form = $this->createFormBuilder()
            ->add('amap', 'entity',
                array(
                    'class' => 'SmlePanBundle:Amap',
                    'property' => 'name'
                )
                )
            ->getForm();

        $newPanierAdherent = new PanierAdherent;
        $formPanier = $this->createForm(new PanierAdherentType, $newPanierAdherent);

        return $this->render('SmlePanBundle:AmapAdherent:index.html.twig', array(
            'entities' => $entities,
            'form' => $form->createView(),
            'form_panier' => $formPanier->createView()
        ));
    }


    /**
     * Assign a Adherent to a Amap.
     *
     */
    public function assignToAction(Request $request)
    {
        $amapAdherent  = new AmapAdherent;
        $newPanierAdherent = new PanierAdherent;
        
        $em = $this->getDoctrine()->getManager();
        $adherents = $request->request->get('adherents');
        
        if (!$adherents) {
            throw $this->createNotFoundException('Unable to find AmapAdherent entity.');
        }

        //if ($form->isValid()) {
        if($request->request->get('assign') == 'amap') {
            $form = $this->createFormBuilder($amapAdherent)
                ->add('amap', 'entity',
                    array(
                        'class' => 'SmlePanBundle:Amap',
                        'property' => 'name'
                    )
                    )
                ->getForm();

            $form->bind($request);

            foreach($adherents as $adherentId)
            {
                $adherent = $em->getRepository('SmlePanBundle:AmapAdherent')->find($adherentId);
                $adherent->setAmap($amapAdherent->getAmap());
                $em->persist($adherent);
            }
        }
        //}
        else if($request->request->get('assign') == 'contact') {
            foreach($adherents as $adherentId)
            {
                $adherent = $em->getRepository('SmlePanBundle:AmapAdherent')->find($adherentId);
                $adherent->setContact(!$adherent->getContact());
                $em->persist($adherent);
            }
        }
        else {
            $form = $this->createForm(new PanierAdherentType, $newPanierAdherent);
            
            $form->bind($request);
            
            foreach($adherents as $adherentId)
            {
                $panierAdherent  = clone $newPanierAdherent;
                $adherent = $em->getRepository('SmlePanBundle:AmapAdherent')->find($adherentId);
                $today = new \DateTime('today');
                $panierAdherent->setDateStart($today);
                $panierAdherent->setDateEnd(null);
                
                //update last record
                $dfin = clone $today;
                $lastPanierAdherent = $em->getRepository('SmlePanBundle:PanierAdherent')->findBy(array('amapAdherent'=>$adherent, 'date_end' => null));
                if($lastPanierAdherent) {
                    //If type error => just update panier
                    if($lastPanierAdherent[0]->getDateStart() == $today) {
                        $lastPanierAdherent[0]->setPanier($panierAdherent->getPanier());
                        $panierAdherent = $lastPanierAdherent[0];
                    }
                    //else add new Panier
                    else {
                        $lastPanierAdherent[0]->setDateEnd($dfin->modify('-1 Day'));
                        $em->persist($lastPanierAdherent[0]);
                    }
                }
                $adherent->addPanierAdherent($panierAdherent);
                $panierAdherent->setAmapAdherent($adherent);
                $em->persist($panierAdherent);
                $em->persist($adherent);
            }
        }
        
        $em->flush();

        return $this->redirect($this->generateUrl('amapadherent'));
    }
    /**
     * Finds and displays a AmapAdherent entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapAdherent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapAdherent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:AmapAdherent:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new AmapAdherent entity.
     *
     */
    public function newAction()
    {
        $entity = new AmapAdherent();
        $form   = $this->createForm(new AmapAdherentType(), $entity);

        return $this->render('SmlePanBundle:AmapAdherent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new AmapAdherent entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new AmapAdherent();
        $form = $this->createForm(new AmapAdherentType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('amapadherent_show', array('id' => $entity->getId())));
        }

        return $this->render('SmlePanBundle:AmapAdherent:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing AmapAdherent entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapAdherent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapAdherent entity.');
        }

        $editForm = $this->createForm(new AmapAdherentType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:AmapAdherent:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing AmapAdherent entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:AmapAdherent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find AmapAdherent entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new AmapAdherentType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('amapadherent_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:AmapAdherent:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a AmapAdherent entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:AmapAdherent')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find AmapAdherent entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('amapadherent'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
