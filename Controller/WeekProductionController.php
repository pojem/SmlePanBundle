<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\WeekProduction;
use Smle\PanBundle\Form\WeekProductionType;

use Smle\PanBundle\Form\PanierOrderProductUnitType;
use Smle\PanBundle\Entity\PanierOrderProductUnit;

use Symfony\Component\HttpFoundation\Response;

/**
 * WeekProduction controller.
 *
 */
class WeekProductionController extends Controller
{
    /**
     * Lists all WeekProduction entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:WeekProduction')->findAll();

        return $this->render('SmlePanBundle:WeekProduction:index.html.twig', array(
            'entities' => $entities,
        ));
        /*
        return $this->dailyTypeAction('');
        */
    }

    /**
     * Finds and displays a WeekProduction entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:WeekProduction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WeekProduction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:WeekProduction:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Get current week
     *
     */
    private function getCurrentWeek($date)
    {
        if(!$date) {
            $date = new \DateTime('today');
            return $date->modify('-'.(($date->format('w') + 6) % 7).' Days');
        }
        return $date = new \DateTime($date);
    }

    /**
     * Return final weekProduction creation form.
     *
     */
    private function buildFinalCreationForm($entity, $twig)
    {
        $em = $this->getDoctrine()->getManager();

        $productPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findCurrents($entity->getDate());

        if (!$productPrices) {
            throw $this->createNotFoundException('Unable to find ProductPrice entity.');
        }
        
        foreach($productPrices as $productPrice) {
            $panierOrderProductUnit = new PanierOrderProductUnit;
            $panierOrderProductUnit->setProductPrice($productPrice);
            $panierOrderProductUnit->setWeekProduction($entity);
            $entity->addProductUnit($panierOrderProductUnit);
        }

        $form = $this->createForm(new WeekProductionType(), $entity);

        return $this->render('SmlePanBundle:WeekProduction:'.$twig.'.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'productPrices' => $productPrices
        ));
    }

    /**
     * Displays a form to create a new WeekProduction entity.
     *
     */
    public function newAction($date)
    {
		$date = $this->getCurrentWeek($date);
		
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:WeekProduction')->findBy(array('date' => $date, 'prevision' => true));
        if($entity) {
            return $this->editAction($entity[0]->getId());
        }
        
        $entity = new WeekProduction();
        
        $entity->setDate($date);
        $entity->setPrevision(true);
        
        return $this->buildFinalCreationForm($entity, 'new');
    }

    /**
     * Creates a new WeekProduction entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new WeekProduction();
        
        $form = $this->createForm(new WeekProductionType(), $entity);
        $form->bind($request);

        $productPrices = $request->request->get('productPrices');
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $i = 0;
            foreach($entity->getProductUnits() as $popu) {
                //remove product with no quantity
                if($popu->getQuantity() == 0) $entity->removeProductUnit($popu);
                else {
                    $productPrice = $em->getRepository('SmlePanBundle:ProductPrice')->find($productPrices[$i]);
                    $popu->setProductPrice($productPrice);
                    $popu->setWeekProduction($entity);
                    $em->persist($popu);
                }
                $i++;
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('weekproduction_edit', array('id' => $entity->getId())));
        }

        return $this->redirect($this->generateUrl('weekproduction_new', array('date' => $entity->getDate()->format('Y-m-d'))));
    }

    /**
     * Creates a new WeekProduction entity via Ajax.
     *
     */
    public function ajaxCreateAction($date)
    {
        $request = $this->container->get('request');
//        if($request->isXmlHttpRequest())
//        {
            //return new Response('<span>'.$date.'</span>');
            $date = new \DateTime($date);
            
            $em = $this->getDoctrine()->getManager();
            $date->modify('+' .($request->get('jour')). ' Days');
            
            $entity  = $em->getRepository('SmlePanBundle:WeekProduction')->findOneBy(array('date' => $date, 'prevision' => false));

            if (!$entity) {
                $entity  = new WeekProduction();
                $entity->setDate($date);
                $entity->setPrevision(false);
            }
            
            $popu = $entity->getProductUnit($request->get('popu'));
            if (!is_object($popu)) {
                $pp = $em->getRepository('SmlePanBundle:ProductPrice')->find($request->get('popu'));
                $popu = new PanierOrderProductUnit();
                $popu->setWeekProduction($entity);
                $popu->setProductPrice($pp);
                $popu->setQuantity($request->get('qty'));
                $entity->addProductUnit($popu);
                $em->persist($entity);
            }
            else
            {
                $popu->setQuantity($request->get('qty'));
                $em->persist($popu);
            }
            
            $em->flush();
                
            return new Response('<span>'.$popu->getQuantity().'</span>');
//        }

        return new Response('problème...');
    }

    /**
     * Displays a form to edit an existing WeekProduction entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:WeekProduction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WeekProduction entity.');
        }

        // search for available other products (not in panierorder)
        $tpu = array();
        foreach($entity->getProductUnits() as $popu) $tpu[] = $popu->getProductPrice()->getId();
        $otherProductPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findCurrents($entity->getDate(), implode(',', $tpu));
        
        foreach($otherProductPrices as $productPrice) {
                $panierOrderProductUnit = new PanierOrderProductUnit;
                $panierOrderProductUnit->setProductPrice($productPrice);
                $panierOrderProductUnit->setWeekProduction($entity);
                $entity->addProductUnit($panierOrderProductUnit);
        }
        
        $editForm = $this->createForm(new WeekProductionType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:WeekProduction:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing WeekProduction entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:WeekProduction')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find WeekProduction entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new WeekProductionType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $i = 0;
            foreach($entity->getProductUnits() as $popu) {
                //remove product with no quantity
                if($popu->getQuantity() == 0) $entity->removeProductUnit($popu);
                else if(!$popu->getProductPrice()){
                    $productPrice = $em->getRepository('SmlePanBundle:ProductPrice')->find($productPrices[$i]);
                    $popu->setProductPrice($productPrice);
                    $popu->setWeekProduction($entity);
                    $em->persist($popu);
                }
                $i++;
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('weekproduction_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:WeekProduction:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a WeekProduction entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:WeekProduction')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find WeekProduction entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('smle_pan_homepage'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
    
    public function dailyTypeAction ($date) {
		
		$date = $this->getCurrentWeek($date);

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:WeekProduction')->findByDate($date);

        if (!$entity) {
            $entity = new WeekProduction();
            $entity->setDate($date);
            $entity->setPrevision(true);
            
            $em->persist($entity);
            $em->flush();
            //return $this->newAction($date);
            //throw $this->createNotFoundException('Unable to find WeekProduction entity.');
        }

        $entity = $entity[0];

        // search for available other products (not in panierorder)
        $tpu = array();
        foreach($entity->getProductUnits() as $popu) $tpu[] = $popu->getProductPrice()->getId();
        $otherProductPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findCurrents($entity->getDate(), implode(',', $tpu));
        
        foreach($otherProductPrices as $productPrice) {
                $panierOrderProductUnit = new PanierOrderProductUnit;
                $panierOrderProductUnit->setProductPrice($productPrice);
                $panierOrderProductUnit->setWeekProduction($entity);
                $entity->addProductUnit($panierOrderProductUnit);
        }

        // get all daily production
        $tdp = array();
        $dailyProductions = $em->getRepository('SmlePanBundle:WeekProduction')->findDailyProduction($entity->getDate());
        foreach($dailyProductions as $dp) {
            foreach($dp->getProductUnits() as $popu) {
                $tdp[$popu->getProductPrice()->getId()][$dp->getDate()->format('w')] = $popu->getQuantity();
            }
        }

        //$editForm = $this->createForm(new WeekProductionType(), $entity);
        //$deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:WeekProduction:dailyEdit.html.twig', array(
            'entity'      => $entity,
            'tdp'         => $tdp
            //'form'   => $editForm->createView(),
            //'delete_form' => $deleteForm->createView(),
        ));
    }
}
