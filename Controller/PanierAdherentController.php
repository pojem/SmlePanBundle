<?php

namespace Smle\PanBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\PanierAdherent;

/**
 * PanierAdherent controller.
 *
 */
class PanierAdherentController extends Controller
{
    /**
     * Lists all PanierAdherent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:PanierAdherent')->findAll();

        return $this->render('SmlePanBundle:PanierAdherent:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a PanierAdherent entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierAdherent')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierAdherent entity.');
        }

        return $this->render('SmlePanBundle:PanierAdherent:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

}
