<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\Panier;
use Smle\PanBundle\Form\PanierType;
use Smle\PanBundle\Entity\PanierPrice;


/**
 * Panier controller.
 *
 */
class PanierController extends Controller
{
    /**
     * Lists all Panier entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:Panier')->findAll();
        
        foreach($entities as &$entity) {
            $date = new \DateTime('now');
            $price = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getId(), $date->format('Y-m-d'));
            if(!$price) {
                $price = new PanierPrice;
                $price->setDateStart(new \DateTime('today'));
            }
            $entity->addPanierPrice($price);
        }

        return $this->render('SmlePanBundle:Panier:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a Panier entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:Panier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Panier entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:Panier:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to create a new Panier entity.
     *
     */
    public function newAction()
    {
        $entity = new Panier();
        $form   = $this->createForm(new PanierType(), $entity);

        return $this->render('SmlePanBundle:Panier:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Creates a new Panier entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Panier();
        $form = $this->createForm(new PanierType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panier_show', array('id' => $entity->getId())));
        }

        return $this->render('SmlePanBundle:Panier:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Panier entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:Panier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Panier entity.');
        }

        $editForm = $this->createForm(new PanierType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:Panier:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Panier entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:Panier')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Panier entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PanierType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panier_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:Panier:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Panier entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:Panier')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Panier entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panier'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
