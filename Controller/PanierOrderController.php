<?php

namespace Smle\PanBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Smle\PanBundle\Entity\PanierOrder;
use Smle\PanBundle\Form\PanierOrderType;
use Smle\PanBundle\Form\PanierAdherentType;
use Smle\PanBundle\Form\PanierOrderProductUnitType;
use Smle\PanBundle\Form\ProductUnitType;
use Smle\PanBundle\Entity\PanierOrderProductUnit;

use Symfony\Component\HttpFoundation\Response;

/**
 * PanierOrder controller.
 *
 */
class PanierOrderController extends Controller
{
    /**
     * Lists all PanierOrder entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('SmlePanBundle:PanierOrder')->findBy(
            array(),
            array('date' => 'DESC'),
            20
            );
            
        foreach($entities as $entity) {
            foreach($entity->getPanierAdherents() as $pa) {
                $entity->addAmap($pa->getAmapAdherent()->getAmap()->getId(), $pa->getAmapAdherent()->getAmap()->getName());
            }
        }

        return $this->render('SmlePanBundle:PanierOrder:index.html.twig', array(
            'entities' => $entities,
        ));
    }

    /**
     * Finds and displays a PanierOrder entity.
     *
     */
    public function searchAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            
            $entities = $em->getRepository('SmlePanBundle:PanierOrder')->findAmapLasts(
                    $request->request->get('amapId'),
                    $request->request->get('type'),
                    $request->request->get('id'),
                    $request->request->get('limit')
                    );
                    
            //return new Response($entities);
            
            if(!$entities) return new Response('aucun panier trouvé ...');

            foreach($entities as &$entity) {
                $panierPrice = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getPanier()->getId(), $entity->getDate());
                $entity->setPanierPrice($panierPrice);
                foreach($entity->getProductUnits() as $popu)
                    $entity->addPanierPriceTotal($popu->getQuantity() * $popu->getProductPrice()->getPrice());
            }
            
            $amap= $em->getRepository('SmlePanBundle:Amap')->find($request->request->get('amapId'));
            
            return $this->render('SmlePanBundle:PanierOrder:search.html.twig', array(
                'entities' => $entities,
                'amap' => $amap
            ));
        }
        throw $this->createNotFoundException('Page non trouvée.');
            
        
    }

    /**
     * Finds and displays a PanierOrder entity.
     *
     */
    public function showAction($id, $amapId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);
        $amap = $amapId > 0 ? $em->getRepository('SmlePanBundle:Amap')->find($amapId) : null;

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }

        $panierPrice = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getPanier()->getId(), $entity->getDate());

        return $this->render('SmlePanBundle:PanierOrder:show.html.twig', array(
            'entity'      => $entity,
            'amap_id' => $amapId,
            'amap' => $amap,
            'panier_price' => $panierPrice
        ));
    }

    /**
     * Displays a form to create a new PanierOrder entity.
     *
     */
    public function newAction($amapId)
    {
        $entity = new PanierOrder();
        $form   = $this->createForm(new PanierOrderType(), $entity);

        if($amapId) {
            $em = $this->getDoctrine()->getManager();

            $amap = $em->getRepository('SmlePanBundle:Amap')->find($amapId);
        }
        else {
            $amapId = 0;
            $amap = null;
        }

        return $this->render('SmlePanBundle:PanierOrder:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'amap' => $amap,
            'amapId' => $amapId
        ));
    }

    /**
     * New step 2, get the panier.
     *
     */
    public function getPanierAction(Request $request)
    {
        $entity  = new PanierOrder();
        $form = $this->createForm(new PanierOrderType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $rpa = $em->getRepository('SmlePanBundle:Panier');
            $amapId = $entity->getAmap()->getId();
            
            $form_panierAdherent = $this->createFormBuilder()
                ->add('panier', 'entity',
                    array(
                        'class' => 'SmlePanBundle:Panier',
                        'property' => 'name',
                        'query_builder' => function(\Smle\PanBundle\Entity\PanierRepository $rpa) use ($amapId) {
                                                return $rpa->findAmapPaniers($amapId);
                                            }
                    )
                    )
                ->getForm();
                
            return $this->render('SmlePanBundle:PanierOrder:newStep3.html.twig', array(
                'entity' => $entity,
                'form'   => $form_panierAdherent->createView(),
            ));
        }
        return $this->render('SmlePanBundle:PanierOrder:newStep3.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * New step 2, change the week via ajax.
     *
     */
    public function changeWeekAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $date = new \DateTime($request->request->get('date'));
            $date->modify($request->request->get('sign').'1 Week');
            return $this->render('SmlePanBundle:PanierOrder:newWeekUpdate.html.twig', array(
                'date' => $date
            ));
        }
        throw $this->createNotFoundException('Page non trouvée.');
    }

    /**
     * Return final panierOrder creation form.
     *
     */
    private function buildFinalCreationForm($entity, $amapId)
    {
        $em = $this->getDoctrine()->getManager();

        $amap = $amapId ?
            $em->getRepository('SmlePanBundle:Amap')->find($amapId) :
            null;

        $productPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findCurrents($entity->getDate());

        $panierPrice = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getPanier()->getId(), $entity->getDate());

        if (!$productPrices) {
            throw $this->createNotFoundException('Unable to find ProductPrice entity.');
        }
        
        foreach($productPrices as $productPrice) {
            $panierOrderProductUnit = new PanierOrderProductUnit;
            $panierOrderProductUnit->setPanierOrder($entity);
            $entity->addProductUnit($panierOrderProductUnit);
        }

        $form = $this->createForm(new PanierOrderType(), $entity);

        // Search ajax form
        $amapAll = $em->getRepository('SmlePanBundle:Amap')->findAll();
        $panierAll = $em->getRepository('SmlePanBundle:Panier')->findAll();
        
        return $this->render('SmlePanBundle:PanierOrder:newStep3.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'productPrices' => $productPrices,
            'panier_price' => $panierPrice,
            'amap' => $amap,
            'amap_id' => $amapId,
            'amap_all' => $amapAll,
            'panier_all' => $panierAll
        ));
    }

    /**
     * New step 3, get the products available for the specified period.
     *
     */
    public function getProductUnitsAction(Request $request, $amapId)
    {
        $entity  = new PanierOrder();
        $form = $this->createForm(new PanierOrderType(), $entity);
        $form->bind($request);
        
        if ($form->isValid()) {
            return $this->buildFinalCreationForm($entity, $amapId);
        }

        return $this->render('SmlePanBundle:PanierOrder:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * get directly the final creation form.
     *
     */
    public function getProductUnitsDirectAction($amapId, $date, $panierId)
    {
        $entity  = new PanierOrder();
        
        $em = $this->getDoctrine()->getManager();
        
        $panier = $em->getRepository('SmlePanBundle:Panier')->find($panierId);
        $entity->setDate(new \DateTime($date))->setPanier($panier);
        
        return $this->buildFinalCreationForm($entity, $amapId);
    }

    /**
     * Creates a new PanierOrder entity.
     *
     */
    public function editAmapPanierAdherentsAction($id, $amap)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }
        
        $panierAdherents = $em->getRepository('SmlePanBundle:PanierAdherent')
                    ->findAmapPanierAdherents($entity->getPanier()->getId(), $amap);
        
        return $this->render('SmlePanBundle:PanierOrder:editamappanieradherents.html.twig', array(
            'entity' => $entity,
            'panier_adherents' => $panierAdherents
        ));

    }

    /**
     * Creates a new PanierOrder entity.
     *
     */
    public function updateAmapPanierAdherentsAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }

        if($panierAdherentsYes = $request->request->get('panierAdherentsYes')) {
        
            foreach($panierAdherentsYes as $panierAdherentId) {
                $panierAdherent = $em->getRepository('SmlePanBundle:PanierAdherent')->find($panierAdherentId);
                if(!$entity->findPanierAdherent($panierAdherent)) $entity->addPanierAdherent($panierAdherent);
            }
        }

        if($panierAdherentsNo = $request->request->get('panierAdherentsNo')) {
        
            foreach($panierAdherentsNo as $panierAdherentId) {
                $panierAdherent = $em->getRepository('SmlePanBundle:PanierAdherent')->find($panierAdherentId);
                if($entity->findPanierAdherent($panierAdherent)){
                    $entity->removePanierAdherent($panierAdherent);
                }
            }
        }

        $em->persist($entity);
        $em->flush();

        return $this->redirect($this->generateUrl('panierorder_edit', array('id' => $entity->getId())));
    }

    /**
     * Creates a new PanierOrder entity.
     *
     */
    public function createAction(Request $request, $amapId)
    {
        $entity  = new PanierOrder();

        $form = $this->createForm(new PanierOrderType(), $entity);
        $form->bind($request);
        
        $productPrices = $request->request->get('productPrices');
        
        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $i = 0;
            foreach($entity->getProductUnits() as $popu) {
                //remove product with no quantity
                if(!$popu->getQuantity()) $entity->removeProductUnit($popu);
                else {
                    $productPrice = $em->getRepository('SmlePanBundle:ProductPrice')->find($productPrices[$i]);
                    $popu->setProductPrice($productPrice);
                    $popu->setPanierOrder($entity);
                    $em->persist($popu);
                }
                $i++;
            }
            if($amapId) {
                $panierAdherents = $em->getRepository('SmlePanBundle:PanierAdherent')->findAmapPanierAdherents($entity->getPanier()->getId(), $amapId);
                foreach($panierAdherents as $panierAdherent) {
                    $entity->addPanierAdherent($panierAdherent);
                }
            }
            else $amapId = 0;

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panierorder_edit', array('id' => $entity->getId(), 'amapId' => $amapId)));
        }

        return $this->render('SmlePanBundle:PanierOrder:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Get the duplicate form.
     *
     */
    public function getDuplicateForm($amapId, $date, $po_id)
    {
        $em = $this->getDoctrine()->getManager();
        
        // Get the number of panierAdherents per amap
        $conn = $this->container->get('database_connection');
        $sql = "SELECT DISTINCT po.panier_id FROM ((pan_panier_order po INNER JOIN pan_panier_order_adherent poa ON po.id = poa.panierorder_id) INNER JOIN pan_panier_adherent pa ON poa.panieradherent_id = pa.id) INNER JOIN pan_amap_adherent aa ON pa.amap_adherent_id = aa.id WHERE po.date = '".$date."' AND aa.amap_id = ".$amapId;
        $paniers = $conn->query($sql);
        
        $excepts = array();
        
        foreach($paniers as $row) $excepts[] = $row['panier_id'];
        $excepts = implode(',', $excepts);

        $form = $this->createFormBuilder()
            ->add('panier', 'entity',
                array(
                    'class' => 'SmlePanBundle:Panier',
                    'property' => 'name',
                    'label' => 'Dupliquer vers ',
                    'empty_value' => '',
                    'query_builder' => function(\Smle\PanBundle\Entity\PanierRepository $rpa) use ($amapId, $excepts) {
                                            return $rpa->findAmapDateOtherPaniers($amapId, $excepts);
                                        }
                ))
            ->add('poRefId', 'hidden', array('data' => $po_id))
            ->add('duplicMode', 'choice', array(
				'choices' => array('1' => 'ajuster les quantités'),
				'data' => array('1', true),
				'multiple' => true,
				'expanded' => true
				))
            ->getForm();

		return $form;
    }

    /**
     * Duplicates a PanierOrder entity to another for the same AMAP and same date.
     *
     */
    public function duplicateAction(Request $request, $amapId, $date, $panierId)
    {
        $entity  = new PanierOrder();

        $em = $this->getDoctrine()->getManager();
        
        $entity->setDate(new \DateTime($date));

        $postData = $request->get('form');

        if(!$panierId) $panierId = $postData['panier'];
        $poRefId = !isset($postData['poRefId']) ? $request->get('poRefId') : $postData['poRefId'];
        $duplicMode = !$request->get('duplicMode') ? isset($postData['duplicMode']) : true;
//print_r($duplicMode);die();
        
        $panierOrderRef = $em->getRepository('SmlePanBundle:PanierOrder')->find($poRefId);
        
        $panier = $em->getRepository('SmlePanBundle:Panier')->find($panierId);
        
        $entity->setPanier($panier);
        
        $amap = $em->getRepository('SmlePanBundle:Amap')->find($amapId);
        
        $panierPriceOld = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($panierOrderRef->getPanier()->getId(), $panierOrderRef->getDate()->format('Y-m-d'));
        $panierPriceNew = $duplicMode ?
            $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($panierId, $date) :
            $panierPriceOld;
        
/*
echo "<pre>";print_r($panierPriceOld->getPrice().",".$panierPriceNew->getPrice());echo "</pre>";
die();
*/

        foreach($panierOrderRef->getProductUnits() as $popu) {
            $popuNew = new PanierOrderProductUnit;
            $popuNew->setProductPrice($popu->getProductPrice());
            $popuNew->setPanierOrder($entity);
            $popuNew->setQuantity($popu->getQuantity() * $panierPriceNew->getPrice() / $panierPriceOld->getPrice());
            $entity->addProductUnit($popuNew);
        }

        $panierAdherents = $em->getRepository('SmlePanBundle:PanierAdherent')->findAmapPanierAdherents($entity->getPanier()->getId(), $amapId);
        foreach($panierAdherents as $panierAdherent) {
            $entity->addPanierAdherent($panierAdherent);
        }

        $em->persist($entity);
        $em->flush();
        return $this->redirect($this->generateUrl('panierorder_edit', array('id' => $entity->getId(), 'amapId' => $amapId)));
    }

    /**
     * Displays a form to edit an existing PanierOrder entity.
     *
     */
    public function createFormPanierAmaps($panierId)
    {
        return $this->createFormBuilder(array('amap' => 0))
                ->add('amap', 'entity',
                    array(
                        'class' => 'SmlePanBundle:Amap',
                        'property' => 'name',
                        'query_builder' => function(\Smle\PanBundle\Entity\AmapRepository $ra) use ($panierId) {
                                                return $ra->findPanierAmaps($panierId);
                                            }
                    )
                    )
                ->getForm();
    }

    /**
     * Displays a form to edit an existing PanierOrder entity.
     *
     */
    private function getFullPanier($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }

        $panierPrice = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getPanier()->getId(), $entity->getDate());

        return $entity;

    }

    /**
     * Print a recap of all paniers for a amap and for a week.
     *
     */
    public function ajaxPreviewAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $em = $this->getDoctrine()->getManager();
            
            $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($request->request->get('id'));
                    
            if(!$entity) return new Response('aucun panier trouvé ...');

            return $this->render('SmlePanBundle:PanierOrder:preview.html.twig', array(
                'entity' => $entity
            ));
        }
        throw $this->createNotFoundException('Page non trouvée.');
    }

    /**
     * Print a recap of all paniers for a amap and for a week.
     *
     */
    public function searchRecapAction(Request $request)
    {
        if($request->isXmlHttpRequest()) {
            $amapId = $request->request->get('amapId');
            
            $em = $this->getDoctrine()->getManager();

            $amap = $em->getRepository('SmlePanBundle:Amap')->find($amapId);

            $conn = $this->container->get('database_connection');
            $sql = "SELECT po.date AS date 
                    FROM ((pan_panier_order po INNER JOIN pan_panier_order_adherent poa ON po.id = poa.panierorder_id) 
                    INNER JOIN pan_panier_adherent pa ON pa.id = poa.panieradherent_id) 
                    INNER JOIN pan_amap_adherent aa ON aa.id = pa.amap_adherent_id 
                    WHERE aa.amap_id = ".$amapId." 
                    GROUP BY po.date 
                    ORDER BY po.date";
            $results = $conn->query($sql);
            
            $today = new \DateTime('today');
            
            $recaps = array();
            foreach($results as $result) {
                $recaps[] = array(
                    'date' => new \DateTime($result['date']),
                    'select' => false
                    );
            }
            
            $recaps[count($recaps)-1]['select'] = true;
            $curdate = $recaps[count($recaps)-1]['date'];
            for($i=count($recaps)-2; $recaps[$i]; $i--) {
                if($recaps[$i]['date'] < $today) {
                    break;
                }
                $recaps[$i]['select'] = true;
                $recaps[$i+1]['select'] = false;
                $curdate = $recaps[$i]['date'];
            }
            
            return $this->render('SmlePanBundle:PanierOrder:searchrecap.html.twig', array(
                'amapId' => $amapId,
                'recaps' => $recaps,
                'curdate' => $curdate->format('Y-m-d')
            ));
        }
        return new Response('erreur inattendue [ajax params expected]');
    }

    /**
     * Print a pdf recap of all paniers for a amap and for a week.
     *
     */
    public function printRecapToPdfAction($amapId, $date)
    {
        $pageUrl = $this->generateUrl('panierorder_printrecapbeforepdf', array(
            'amapId' => $amapId,
            'date' => $date
            ), true);
            
        //return $this->redirect($pageUrl);

        return new Response(
            $this->get('knp_snappy.pdf')->getOutput($pageUrl, array('print-media-type' => true)),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'attachment; filename="recap'.$amapId.'-'.$date.'pdf"'
            )
        );
        
    }

    /**
     * Print a recap of all paniers for a amap and for a week.
     *
     */
    public function printRecapAction($amapId, $date, $media = 'screen')
    {
        $em = $this->getDoctrine()->getManager();

        $amap = $em->getRepository('SmlePanBundle:Amap')->find($amapId);
        $amapDeliveryDay = $em->getRepository('SmlePanBundle:AmapDeliveryDay')->findLast($amapId, $date);
        if(!$amapDeliveryDay) {
            return new Response('Jour de livraison non défini');
        }
        $amap->addAmapDeliveryDay($amapDeliveryDay);

        $conn = $this->container->get('database_connection');
        $sql = "SELECT po.id AS po_id, count(*) AS t_adh, p.label AS p_name 
                FROM (((pan_panier_order po INNER JOIN pan_panier_order_adherent poa ON po.id = poa.panierorder_id) 
                INNER JOIN pan_panier_adherent pa ON pa.id = poa.panieradherent_id) 
                INNER JOIN view_pan_panier p ON p.id = pa.panier_id) 
                INNER JOIN pan_amap_adherent aa ON aa.id = pa.amap_adherent_id 
                WHERE aa.amap_id = ".$amapId." AND po.date = '".$date."' 
                GROUP BY po.id, p.ref, p.label 
                ORDER BY p.ref";
        $amapPanierOrders = $conn->query($sql);
        
        $dateDelivery = new \Datetime($date);
        $dateDelivery->modify('+'.($amapDeliveryDay->getDay() - 1).' Days');
        
        $tResults = array();
        
        foreach($amapPanierOrders as $row) {
            $tResults[$row['po_id']] = array(
                'nadherents' => $row['t_adh'],
                'name' => $row['p_name']
                );
        }
        
        $tProducts = array();

        foreach($tResults as $po => $results) {
            $panierOrder = $this->getFullPanier($po);
            foreach($panierOrder->getProductUnits() as $productUnit) {
//echo "<pre>";print_r($productUnit->getPanierOrder()->getId());echo "</pre>";
                $productId = $productUnit->getProductPrice()->getProductUnit()->getProduct()->getId();
                if(!isset($tProducts[$productId]['tqty'])) $tProducts[$productId]['tqty'] = 0;
                $tProducts[$productId]['panier'][$po] = $productUnit;
                $tProducts[$productId]['name'] = $productUnit->getProductPrice()->getProductUnit()->getProduct()->getLabel();
                $tProducts[$productId]['tqty'] += $productUnit->getQuantity() * $tResults[$po]['nadherents'];
                
            }
        }
//die($media);  
        
        return $this->render('SmlePanBundle:PanierOrder:printrecap.html.twig', array(
            'results' => $tResults,
            'products' => $tProducts,
            'amap' => $amap,
            'date' => $dateDelivery,
            'dateUrl' => $date,
            'media' => $media
        ));
        
    }

    /**
     * Displays a form to edit an existing PanierOrder entity.
     *
     */
    public function editAction($id, $amapId)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }

        $panierPrice = $em->getRepository('SmlePanBundle:PanierPrice')->findAllCurrent($entity->getPanier()->getId(), $entity->getDate());
//echo "<pre>";print_r($panierPrice->getPrice());echo "</pre>";die();

        // search for available other products (not in panierorder)
        $tpu = array();
        foreach($entity->getProductUnits() as $popu) $tpu[] = $popu->getProductPrice()->getId();
        $otherProductPrices = $em->getRepository('SmlePanBundle:ProductPrice')->findCurrents($entity->getDate(), implode(',', $tpu));
        
        foreach($otherProductPrices as $productPrice) {
                $panierOrderProductUnit = new PanierOrderProductUnit;
                $panierOrderProductUnit->setProductPrice($productPrice);
                $panierOrderProductUnit->setPanierOrder($entity);
                $entity->addProductUnit($panierOrderProductUnit);
        }

        //AmapAdherent association form, filter the Amaps involved by this panier
        $panierId = $entity->getPanier()->getId();

        //Form build to the duplicate action to other Paniers
        $form_panier = $this->getDuplicateForm($amapId, $entity->getDate()->format('Y-m-d'), $id);
        
        // Get the amaps affected by this panier
        $amaps = $em->getRepository('SmlePanBundle:Amap')->findPanierAmaps($panierId);
        
        // Get the number of panierAdherents per amap
        $conn = $this->container->get('database_connection');
        $sql = 'SELECT * FROM pan_view_panierorderamapadherent WHERE po_id ='.$entity->getId();
        $panierOrderAmapAdherentStat = $conn->query($sql);
        
        // Get the number of adherents per amap
        $sql = 'SELECT * FROM pan_view_panieramapadherent WHERE p_id ='.$panierId;
        $panierAmapAdherentStat = $conn->query($sql);
        
        $tAmapStats = array();
        foreach($panierAmapAdherentStat as $row)
        {
            $tAmapStats[$row['a_id']] = $row;
            $tAmapStats[$row['a_id']]['npanieramapadherents']  = 0;
        }

        foreach($panierOrderAmapAdherentStat as $row)
        {
            if(isset($tAmapStats[$row['a_id']]))
                $tAmapStats[$row['a_id']]['npanieramapadherents'] = $row['namapadherents'];
        }

        // Search ajax form
        $amapAll = $em->getRepository('SmlePanBundle:Amap')->findAll();
        $panierAll = $em->getRepository('SmlePanBundle:Panier')->findAll();

        $editForm = $this->createForm(new PanierOrderType(), $entity);

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('SmlePanBundle:PanierOrder:edit.html.twig', array(
            'entity'      => $entity,
            'form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'panier_form' => $form_panier->createView(),
            'amap_data' => $tAmapStats,
            'amaps' => $amaps,
            'amap_id' => $amapId,
            'panier_price' => $panierPrice,
            'amap_all' => $amapAll,
            'panier_all' => $panierAll
        ));
    }

    /**
     * Edits an existing PanierOrder entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find PanierOrder entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new PanierOrderType(), $entity);
        $editForm->bind($request);

        $productPrices = $request->request->get('productPrices');

        if ($editForm->isValid()) {
            $i = 0;
            foreach($entity->getProductUnits() as $popu) {
                if($popu->getQuantity() == 0) {
                    $entity->removeProductUnit($popu);
                    $em->remove($popu);
                }
                else {
                    $productPrice = $em->getRepository('SmlePanBundle:ProductPrice')->find($productPrices[$i]);
                    $popu->setProductPrice($productPrice);
                    $popu->setPanierOrder($entity);
                }
                $i++;
            }
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('panierorder_edit', array('id' => $id)));
        }

        return $this->render('SmlePanBundle:PanierOrder:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a PanierOrder entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('SmlePanBundle:PanierOrder')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find PanierOrder entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('panierorder'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
