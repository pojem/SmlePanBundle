<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\Unit
 *
 * @ORM\Table("pan_unit")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\UnitRepository")
 */
class Unit
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", length=20)
     */
    private $label;

	/**
	 * @ORM\OneToMany(targetEntity="ProductUnit", mappedBy="vacation")
	 */
	private $productUnits;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Unit
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     * @return Unit
     */
    public function addProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits[] = $productUnits;
    
        return $this;
    }

    /**
     * Remove productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     */
    public function removeProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits->removeElement($productUnits);
    }

    /**
     * Get productUnits
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductUnits()
    {
        return $this->productUnits;
    }
}