<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\ProductPrice
 *
 * @ORM\Table("pan_product_price")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\ProductPriceRepository")
 */
class ProductPrice
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $dStart
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $dStart;

    /**
     * @var \DateTime $dEnd
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dEnd;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price", type="decimal", precision=10, scale=2)
     */
    private $price;

	/**
	 * @ORM\ManyToOne(targetEntity="Smle\PanBundle\Entity\ProductUnit")
	 * @ORM\JoinColumn(nullable=false)
	 */
	private $productUnit;


    public function __construct()
    {
        $this->dStart = new \DateTime('today');
    }
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param float $price
     * @return ProductPrice
     */
    public function setId($id)
    {
        $this->id = $id;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set dStart
     *
     * @param \DateTime $dStart
     * @return ProductPrice
     */
    public function setDStart($dStart)
    {
        $this->dStart = $dStart;
    
        return $this;
    }

    /**
     * Get dStart
     *
     * @return \DateTime 
     */
    public function getDStart()
    {
        return $this->dStart;
    }

    /**
     * Set dEnd
     *
     * @param \DateTime $dEnd
     * @return ProductPrice
     */
    public function setDEnd($dEnd)
    {
        $this->dEnd = $dEnd;
    
        return $this;
    }

    /**
     * Get dEnd
     *
     * @return \DateTime 
     */
    public function getDEnd()
    {
        return $this->dEnd;
    }

    /**
     * Set productUnit
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnit
     * @return ProductPrice
     */
    public function setProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnit)
    {
        $this->productUnit = $productUnit;
    
        return $this;
    }

    /**
     * Get productUnit
     *
     * @return Smle\PanBundle\Entity\ProductUnit 
     */
    public function getProductUnit()
    {
        return $this->productUnit;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return ProductPrice
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }
}