<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\AmapAdherent
 *
 * @ORM\Table("pan_amap_adherent")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\AmapAdherentRepository")
 */
class AmapAdherent
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean $contact
     *
     * @ORM\Column(name="contact", type="boolean")
     */
    private $contact;

    /**
     * @ORM\OneToOne(targetEntity="Smle\PanBundle\Entity\Adherent")
     */
    private $adherent;

	/**
	 * @ORM\ManyToOne(targetEntity="Amap", inversedBy="amapAdherent")
	 * @ORM\JoinColumn(name="amap_id", referencedColumnName="id")
	 */
	private $amap;

	/**
	 */
	//* @ORM\OneToMany(targetEntity="PanierAdherent", mappedBy="amapAdherent")
	private $panierAdherents;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param boolean $contact
     * @return AmapAdherent
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return boolean 
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set adherent
     *
     * @param Smle\PanBundle\Entity\Adherent $adherent
     * @return AmapAdherent
     */
    public function setAdherent(\Smle\PanBundle\Entity\Adherent $adherent = null)
    {
        $this->adherent = $adherent;
    
        return $this;
    }

    /**
     * Get adherent
     *
     * @return Smle\PanBundle\Entity\Adherent 
     */
    public function getAdherent()
    {
        return $this->adherent;
    }

    /**
     * Set amap
     *
     * @param Smle\PanBundle\Entity\Amap $amap
     * @return AmapAdherent
     */
    public function setAmap(\Smle\PanBundle\Entity\Amap $amap = null)
    {
        $this->amap = $amap;
    
        return $this;
    }

    /**
     * Get amap
     *
     * @return Smle\PanBundle\Entity\Amap 
     */
    public function getAmap()
    {
        return $this->amap;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->panierAdherents = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     * @return AmapAdherent
     */
    public function addPanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents[] = $panierAdherents;
    
        return $this;
    }

    /**
     * Remove panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     */
    public function removePanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents->removeElement($panierAdherents);
    }

    /**
     * Get panierAdherents
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierAdherents()
    {
        return $this->panierAdherents;
    }
}
