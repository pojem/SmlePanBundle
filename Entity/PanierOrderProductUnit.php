<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\PanierOrderProductUnit
 *
 * @ORM\Table("pan_panier_order_product_unit")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\PanierOrderProductUnitRepository")
 */
class PanierOrderProductUnit
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float $quantity
     *
     * @ORM\Column(name="quantity", type="float")
     */
    private $quantity;

	/**
	 * @ORM\ManyToOne(targetEntity="ProductPrice", inversedBy="panierOrderProductUnit", cascade={"persist"})
	 * @ORM\JoinColumn(name="product_price_id", referencedColumnName="id")
	 */
	private $productPrice;

	/**
	 * @ORM\ManyToOne(targetEntity="WeekProduction", inversedBy="panierOrderProductUnit")
	 * @ORM\JoinColumn(name="week_production_id", referencedColumnName="id")
	 */
	private $weekProduction;

	/**
	 * @ORM\ManyToOne(targetEntity="PanierOrder", inversedBy="panierOrderProductUnit")
	 * @ORM\JoinColumn(name="panier_order_id", referencedColumnName="id")
	 */
	private $panierOrder;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->quantity = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     * @return PanierOrderProductUnit
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set panierOrder
     *
     * @param Smle\PanBundle\Entity\PanierOrder $panierOrder
     * @return PanierOrderProductUnit
     */
    public function setPanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrder = null)
    {
        $this->panierOrder = $panierOrder;
    
        return $this;
    }

    /**
     * Get panierOrder
     *
     * @return Smle\PanBundle\Entity\PanierOrder 
     */
    public function getPanierOrder()
    {
        return $this->panierOrder;
    }

    /**
     * Set productPrice
     *
     * @param \Smle\PanBundle\Entity\ProductPrice $productPrice
     * @return PanierOrderProductUnit
     */
    public function setProductPrice(\Smle\PanBundle\Entity\ProductPrice $productPrice = null)
    {
        $this->productPrice = $productPrice;
    
        return $this;
    }

    /**
     * Get productPrice
     *
     * @return \Smle\PanBundle\Entity\ProductPrice 
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * Set weekProduction
     *
     * @param \Smle\PanBundle\Entity\WeekProduction $weekProduction
     * @return PanierOrderProductUnit
     */
    public function setWeekProduction(\Smle\PanBundle\Entity\WeekProduction $weekProduction = null)
    {
        $this->weekProduction = $weekProduction;
    
        return $this;
    }

    /**
     * Get weekProduction
     *
     * @return \Smle\PanBundle\Entity\WeekProduction 
     */
    public function getWeekProduction()
    {
        return $this->weekProduction;
    }
}