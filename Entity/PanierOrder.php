<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\PanierOrder
 *
 * @ORM\Table("pan_panier_order")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\PanierOrderRepository")
 */
class PanierOrder
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime $date
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var integer $status
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @ORM\ManyToMany(targetEntity="PanierAdherent", cascade={"persist"})
	 * @ORM\JoinColumn(name="panierorder_id", referencedColumnName="id")
     * @ORM\JoinTable(name="pan_panier_order_adherent")
     */
    private $panierAdherents;

	/**
	 * @ORM\ManyToOne(targetEntity="Panier", inversedBy="panierOrder")
	 * @ORM\JoinColumn(name="panier_id", referencedColumnName="id")
	 */
	private $panier;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\PanierOrderProductUnit", mappedBy="panierOrder", cascade={"persist", "remove"})
	 */
	private $productUnits;

	/**
     * 
	 */
	private $panierPrice;
    
    private $nbPanierAdherents;
    
    private $tAmaps;
    
    private $panierPriceTotal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return PanierOrder
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }


    /**
     * Add productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     * @return PanierOrder
     */
    public function addProductUnit(\Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits)
    {
        $this->productUnits[] = $productUnits;
    
        return $this;
    }

    /**
     * Remove productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     */
    public function removeProductUnit(\Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits)
    {
        $this->productUnits->removeElement($productUnits);
    }

    /**
     * Get productUnits
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductUnits()
    {
        return $this->productUnits;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->panierAdherents = new \Doctrine\Common\Collections\ArrayCollection();
        $this->productUnits = new \Doctrine\Common\Collections\ArrayCollection();
        $date = new \DateTime('today');
        $day = $date->format('w');
        $this->date = $date->modify('+'.(7 * ($day > 1) - $day + 1).' Days');
        $this->status = 0;
        $this->panierPriceTotal = 0;
    }
    
    /**
     * Add panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     * @return PanierOrder
     */
    public function addPanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents[] = $panierAdherents;
    
        return $this;
    }

    /**
     * Remove panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     */
    public function removePanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents->removeElement($panierAdherents);
    }

    /**
     * Get panierAdherents
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierAdherents()
    {
        return $this->panierAdherents;
    }

    /**
     * Find panierAdherent
     *
     * @return boolean
     */
    public function findPanierAdherent(PanierAdherent $panierAdherent)
    {
        foreach($this->getPanierAdherents() as $pa) {
            if($pa == $panierAdherent) return true;
        }
        return false;
    }

    /**
     * Set panier
     *
     * @param Smle\PanBundle\Entity\Panier $panier
     * @return PanierOrder
     */
    public function setPanier(\Smle\PanBundle\Entity\Panier $panier = null)
    {
        $this->panier = $panier;
    
        return $this;
    }

    /**
     * Get panier
     *
     * @return Smle\PanBundle\Entity\Panier 
     */
    public function getPanier()
    {
        return $this->panier;
    }

    /**
     * Get nbPanierAdherents
     *
     * @return integer 
     */
    public function getNbPanierAdherents()
    {
        return $this->nbPanierAdherents;
    }

    /**
     * Set nbPanierAdherents
     *
     * @return PanierOrder 
     */
    public function setNbPanierAdherents($nbPanierAdherent)
    {
        $this->nbPanierAdherents = $nbPanierAdherent;
    
        return $this;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return PanierOrder
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    /**
     * Set Affected Amaps
     * 
     */
    public function addAmap($id, $name) {
        $this->tAmaps[$id] = $name;
        
        return $this;
    }
    
    /**
     * Get Affected Amaps
     * 
     */
    public function getAmaps() {
        return $this->tAmaps;
    }
        

    /**
     * Set panierPrice
     *
     * @param \Smle\PanBundle\Entity\PanierPrice $panierPrice
     * @return PanierOrder
     */
    public function setPanierPrice(\Smle\PanBundle\Entity\PanierPrice $panierPrice = null)
    {
        $this->panierPrice = $panierPrice;
    
        return $this;
    }

    /**
     * Get panierPrice
     *
     * @return \Smle\PanBundle\Entity\PanierPrice 
     */
    public function getPanierPrice()
    {
        return $this->panierPrice;
    }

    /**
     * add panierPriceFinal
     *
     * @param integer
     * @return PanierOrder
     */
    public function addPanierPriceTotal($price)
    {
        $this->panierPriceTotal += $price;
    
        return $this;
    }

    /**
     * Get panierPriceFinal
     *
     * @return integer
     */
    public function getPanierPriceTotal()
    {
        return $this->panierPriceTotal;
    }
}
