<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\AmapDeliveryDay
 *
 * @ORM\Table("pan_amap_deliveryday")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\AmapDeliveryDayRepository")
 */
class AmapDeliveryDay
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var smallint $day
     *
     * @ORM\Column(name="day", type="smallint")
     */
    private $day;

    /**
     * @var \DateTime $date_start
     *
     * @ORM\Column(name="date_start", type="datetime")
     */
    private $date_start;

    /**
	 * @ORM\ManyToOne(targetEntity="Amap", inversedBy="AmapDeliveryDay")
	 * @ORM\JoinColumn(name="amap_id", referencedColumnName="id")
     */
    private $amap;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->date_start = new \DateTime('today');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amap
     *
     * @param Smle\PanBundle\Entity\Amap $amap
     * @return AmapDeliveryDay
     */
    public function setAmap(\Smle\PanBundle\Entity\Amap $amap = null)
    {
        $this->amap = $amap;
    
        return $this;
    }

    /**
     * Get amap
     *
     * @return Smle\PanBundle\Entity\Amap 
     */
    public function getAmap()
    {
        return $this->amap;
    }

    /**
     * Set day
     *
     * @param integer $day
     * @return AmapDeliveryDay
     */
    public function setDay($day)
    {
        $this->day = $day;
    
        return $this;
    }

    /**
     * Get day
     *
     * @return integer 
     */
    public function getDay()
    {
        return $this->day;
    }

    /**
     * Set date_start
     *
     * @param \DateTime $dateStart
     * @return AmapDeliveryDay
     */
    public function setDateStart($dateStart)
    {
        $this->date_start = $dateStart;
    
        return $this;
    }

    /**
     * Get date_start
     *
     * @return \DateTime 
     */
    public function getDateStart()
    {
        return $this->date_start;
    }
}