<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * WeekProduction
 *
 * @ORM\Table("pan_week_production")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\WeekProductionRepository")
 */
class WeekProduction
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\PanierOrderProductUnit", mappedBy="weekProduction", cascade={"persist", "remove"})
	 */
	private $productUnits;

	/**
	 * @var boolean
     * 
     * @ORM\Column(name="prevision", type="boolean", options={"default" : FALSE})
	 */
	private $prevision;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return WeekProduction
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add productUnits
     *
     * @param \Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits
     * @return WeekProduction
     */
    public function addProductUnit(\Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits)
    {
        $this->productUnits[] = $productUnits;
    
        return $this;
    }

    /**
     * Remove productUnits
     *
     * @param \Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits
     */
    public function removeProductUnit(\Smle\PanBundle\Entity\PanierOrderProductUnit $productUnits)
    {
        $this->productUnits->removeElement($productUnits);
    }

    /**
     * Get productUnits
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductUnits()
    {
        return $this->productUnits;
    }

    /**
     * Get total quantity
     *
     * @return float 
     */
    public function getTotalQuantities()
    {
        $total = 0;
        foreach ($this->getProductUnits() as $pu) $total += $pu->getQuantity();
        return $total;
    }

    /**
     * Set prevision
     *
     * @param boolean $prevision
     * @return WeekProduction
     */
    public function setPrevision($prevision)
    {
        $this->prevision = $prevision;
    
        return $this;
    }

    /**
     * Get prevision
     *
     * @return boolean 
     */
    public function getPrevision()
    {
        return $this->prevision;
    }

    /**
     * Get productUnit
     *
     * @return \Smle\PanBundle\Entity\PanierOrderProductUnit 
     */
    public function getProductUnit($id)
    {
        foreach ($this->getProductUnits() as $pu) {
            if( $pu->getProductPrice()->getId() == $id) return $pu;
        }
        return null;
    }
}
