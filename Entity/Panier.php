<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\Panier
 *
 * @ORM\Table("view_pan_panier")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\PanierRepository")
 */

//virtual_pan_panier //
class Panier
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="label", type="string", length=50)
     */
    private $name;

    /**
     * @var string $ref
     *
     * @ORM\Column(name="ref", type="string", length=6, nullable=true)
     */
    private $ref;

    /**
     * @var text $description
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

	/**
	 */
	private $panierOrders;

	/**
	 */
	private $panierPrices;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\PanierAdherent", mappedBy="panier")
	 */
	private $panierAdherents;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Panier
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->panierOrders = new \Doctrine\Common\Collections\ArrayCollection();
        $this->panierPrices = new \Doctrine\Common\Collections\ArrayCollection();
        $this->panierAdherents = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    /**
     * Add panierOrders
     *
     * @param Smle\PanBundle\Entity\PanierOrder $panierOrders
     * @return Panier
     */
    public function addPanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders[] = $panierOrders;
    
        return $this;
    }

    /**
     * Remove panierOrders
     *
     * @param Smle\PanBundle\Entity\PanierOrder $panierOrders
     */
    public function removePanierOrder(\Smle\PanBundle\Entity\PanierOrder $panierOrders)
    {
        $this->panierOrders->removeElement($panierOrders);
    }

    /**
     * Get panierOrders
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierOrders()
    {
        return $this->panierOrders;
    }

    /**
     * Add panierPrices
     *
     * @param Smle\PanBundle\Entity\PanierPrice $panierPrices
     * @return Panier
     */
    public function addPanierPrice(\Smle\PanBundle\Entity\PanierPrice $panierPrices)
    {
        $this->panierPrices[] = $panierPrices;
    
        return $this;
    }

    /**
     * Remove panierPrices
     *
     * @param Smle\PanBundle\Entity\PanierPrice $panierPrices
     */
    public function removePanierPrice(\Smle\PanBundle\Entity\PanierPrice $panierPrices)
    {
        $this->panierPrices->removeElement($panierPrices);
    }

    /**
     * Get panierPrices
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierPrices()
    {
        return $this->panierPrices;
    }

    /**
     * Add panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     * @return Panier
     */
    public function addPanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents[] = $panierAdherents;
    
        return $this;
    }

    /**
     * Remove panierAdherents
     *
     * @param Smle\PanBundle\Entity\PanierAdherent $panierAdherents
     */
    public function removePanierAdherent(\Smle\PanBundle\Entity\PanierAdherent $panierAdherents)
    {
        $this->panierAdherents->removeElement($panierAdherents);
    }

    /**
     * Get panierAdherents
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getPanierAdherents()
    {
        return $this->panierAdherents;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Panier
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Panier
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
