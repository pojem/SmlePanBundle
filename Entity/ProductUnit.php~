<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\ProductUnit
 *
 * @ORM\Table("pan_product_unit")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\ProductUnitRepository")
 */
class ProductUnit
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var float $weight
     *
     * @ORM\Column(name="weight", type="decimal", precision=10, scale=2)
     */
    private $weight;

	/**
	 * @ORM\ManyToOne(targetEntity="Product", inversedBy="productunit")
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
	 */
	private $product;

	/**
	 * @ORM\ManyToOne(targetEntity="Unit", inversedBy="productunit")
	 * @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
	 */
	private $unit;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\ProductPrice", mappedBy="productUnit")
	 */
	private $productPrices;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set weight
     *
     * @param float $weight
     * @return Unit
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
    
        return $this;
    }

    /**
     * Get weight
     *
     * @return float 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set product
     *
     * @param Smle\PanBundle\Entity\Product $product
     * @return ProductUnit
     */
    public function setProduct(\Smle\PanBundle\Entity\Product $product = null)
    {
        $this->product = $product;
    
        return $this;
    }

    /**
     * Get product
     *
     * @return Smle\PanBundle\Entity\Product 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set unit
     *
     * @param Smle\PanBundle\Entity\Unit $unit
     * @return ProductUnit
     */
    public function setUnit(\Smle\PanBundle\Entity\Unit $unit = null)
    {
        $this->unit = $unit;
    
        return $this;
    }

    /**
     * Get unit
     *
     * @return Smle\PanBundle\Entity\Unit 
     */
    public function getUnit()
    {
        return $this->unit;
    }
}
