<?php

namespace Smle\PanBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Smle\PanBundle\Entity\Product
 *
 * @ORM\Table("virtual_pan_product")
 * @ORM\Entity(repositoryClass="Smle\PanBundle\Entity\ProductRepository")
 */
 
// //view_pan_product
class Product
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string $ref
     *
     * @ORM\Column(name="ref", type="string", length=32)
     */
    private $ref;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", length=255)
     */
    private $label;

    /**
     * @var float $price
     *
     * @ORM\Column(name="price_ttc", type="decimal", precision=10, scale=2)
     */
    private $price;

	/**
	 * @ORM\OneToMany(targetEntity="Smle\PanBundle\Entity\ProductUnit", mappedBy="product")
	 */
	private $productUnits;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     * @return Product
     */
    public function setLabel($label)
    {
        $this->label = $label;
    
        return $this;
    }

    /**
     * Get label
     *
     * @return string 
     */
    public function getLabel()
    {
        return $this->label;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productUnits = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Product
     */
    public function setRef($ref)
    {
        $this->ref = $ref;
    
        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Add productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     * @return Product
     */
    public function addProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits[] = $productUnits;
    
        return $this;
    }

    /**
     * Remove productUnits
     *
     * @param Smle\PanBundle\Entity\ProductUnit $productUnits
     */
    public function removeProductUnit(\Smle\PanBundle\Entity\ProductUnit $productUnits)
    {
        $this->productUnits->removeElement($productUnits);
    }

    /**
     * Get productUnits
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getProductUnits()
    {
        return $this->productUnits;
    }
}
