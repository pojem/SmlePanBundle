<?php

//src/Smle/PanBundle/Twig/PanExtension.php
namespace Smle\PanBundle\Twig;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class PanExtension extends \Twig_Extension
{
    private $generator;
    private $dateRef;
    private $rowstart;
    private $dateRefEnd;
    private $coeff;
    private $iMonth;
    
    public function __construct(UrlGeneratorInterface $generator)
    {
        $this->generator = $generator;
    }

    public function getFunctions()
    {
        return array(
            'hasChild' => new \Twig_Function_Method($this, 'hasChild'),
        );
    }

    public function getFilters()
    {
        return array(
            'init' => new \Twig_Filter_Method($this, 'init'),
            'round' => new \Twig_Filter_Method($this, 'round'),
            'getYear' => new \Twig_Filter_Method($this, 'getYear'),
            'getDay' => new \Twig_Filter_Method($this, 'getDay'),
            'getWeek' => new \Twig_Filter_Method($this, 'getWeek'),
            'setYears' => new \Twig_Filter_Method($this, 'setYears', array('is_safe' => array('html'))),
            'toDisplay' => new \Twig_Filter_Method($this, 'toDisplay', array('is_safe' => array('html'))),
            'toEnd' => new \Twig_Filter_Method($this, 'toEnd', array('is_safe' => array('html'))),
            'weekSet' => new \Twig_Filter_Method($this, 'weekSet', array('is_safe' => array('html'))),
            'monthSet' => new \Twig_Filter_Method($this, 'monthSet', array('is_safe' => array('html'))),
        );
    }

    public function init($dateRef, $coeff, $rowstart)
    {
        //Init settings
        $this->dateRef = is_object($dateRef) ? $dateRef : new \DateTime($dateRef);
        $this->rowstart = $rowstart;
        //Index +1 when day of begin week is in previous month
        $this->iMonth = $this->dateRef->format('j') < 7 ? 0 : 1;
        $this->dateRefEnd = clone $this->dateRef;
        $this->dateRefEnd->modify('+1 Year');
        $this->dateRefEnd->modify('-'.($this->dateRefEnd->format('w')-1).' Days');

        $this->coeff = $coeff;

    }

    public function hasChild($array)
    {
        return count($array);
    }

    public function round($number, $precision=0)
    {
        return sprintf("%01.".$precision."f", $number);
    }

    public function setYears($dateRef, $rowstart=0, $coeff=10)
    {
        $this->init($dateRef, $coeff, $rowstart);
        //Set the year link.
        $mLeft = $this->dateRef->format('W') < 2 || $this->dateRef->format('W') > 51 ? 0 : $this->mToInt(12 - ($this->dateRef->format('n') + $this->iMonth - 1));
        $year = $this->dateRef->format('Y') + ($mLeft ? 1 : $this->iMonth);
        $dYear = new \DateTime($year.'-01-01');
        $dYear->modify('-'.($dYear->format('w')-1).' Days');
        $dYearOther = new \DateTime($dYear->format('Y-m-d'));
        if(!$mLeft) $dYear->modify('-364 Days');
        $url = $this->generator->generate('product_date_all', array(
                    'ddeb' => $dYear->format('Y-m-d'),
                    'rowstart' => $this->rowstart
                    ));
        $dYearOther->modify(($mLeft ? '-' : '+').'364 Days');
        $mLeftOther = $mLeft ? 0 : $coeff*52 - 42;
        $urlOther = $this->generator->generate('product_date_all', array(
                    'ddeb' => $dYearOther->format('Y-m-d'),
                    'rowstart' => $this->rowstart
                    ));
        return
            '<div class="wyear" style="margin-left:'.
            $mLeft.
            'px;"><a class="emphasis" href="'.
            $url.
            '">'.
            $year.
            '</a></div>'.
            '<div class="wyear" style="margin-left:'.
            $mLeftOther.
            'px;"><a class="emphasis" href="'.
            $urlOther.
            '">'.
            ($year + ($mLeft ? -1 : 1)).
            '</a></div>'
            ;
    }

    public function getYear($strDate)
    {
        $date = new \DateTime($strDate);
        $date->modify('+7 Days');
        return $date->format('Y');
    }

    public function getWeek($date, $option = null)
    {
        switch($option) {
            case 'full': return 'Semaine '.$date->format('W').' ('.$date->format('d/m/Y').')';
        }
        return $date->format('W');
    }

    public function getDay($n)
    {
        $tDays = array(
                -1 => '(non défini)',
                0 => 'dimanche',
                1 => 'lundi',
                2 => 'mardi',
                3 => 'mercredi',
                4 => 'jeudi',
                5 => 'vendredi',
                6 => 'samedi'
                )
        ;
        return $tDays[$n];
    }

    public function toDisplay($pp)
    {
        return 
            '<div class="wprice" style="margin-left:'.
            $this->wToInt($pp->getDStart()).
            'px;width:'.
            $this->wDiff($pp->getDStart(), $pp->getDEnd()).
            'px"><input type="hidden" value="'.
            $pp->getId().
            '" /><a href="#">'.
            $pp->getPrice().
            '</a></div>';
        
    }

    public function toEnd($pp)
    {
        return 
            '<div class="wnull" style="margin-left:'.
            $this->wToInt($pp->getDStart()).
            'px;width:8px"><input type="hidden" value="'.
            $pp->getId().
            '" /><a href="#">&nbsp;&nbsp;</a></div>';
        
    }

    public function weekSet($index)
    {
        return 
            '<div class="wblock wblock-down" style="width:'.
            ($this->coeff - 1).
            'px;margin-left:'.
            ($this->coeff*$index).
            'px;" title="'.
            $this->wDate($index, true).
            '"><a id="'.$this->wDate($index).'">'.((((int)$this->dateRef->format('W') + $index - 1) % 52) + 1).'</a></div>';
    }

    public function monthSet($index)
    {
        $url = $this->generator->generate('product_date_all', array(
                    'ddeb' => $this->mDate($index),
                    'rowstart' => $this->rowstart
                    ));
        return 
            '<div class="wmonth" style="margin-left:'.
            ($this->mToInt($index)).
            'px;"><a href="'.
            $url.
            '">'.
            $this->mToStr($index).
            '</a></div>';
        
    }

    private function mToInt($index)
    {
		$date = clone $this->dateRef;
        $newDate = new \DateTime;
        $newDate->setDate($date->format('Y'), $date->format('n') + $index + $this->iMonth, 1);
        
        return (int)($date->diff($newDate)->format('%r%a')*$this->coeff*52/365);
    }

    private function mToStr($index)
    {
		$date = clone $this->dateRef;
		$wMonths = array(
            'jan', 'fév', 'mar', 'avr', 'mai', 'jun', 'jul', 'aou', 'sep', 'oct', 'nov', 'dec'
            );
        
        $month = ($date->format('n') + $index + $this->iMonth -1) % 12;
        
        return $wMonths[$month];
    }

    private function mDate($index)
    {
        $newDate = new \DateTime;
        $newDate->setDate($this->dateRef->format('Y'), $this->dateRef->format('n') + $index + $this->iMonth, 1);
        $newDate->modify('-'.($newDate->format('w')-1).' Days');
        return $newDate->modify('-182 Days')->format('Y-m-d');
    }

    private function wToInt($date)
    {
        if($date < $this->dateRef) return 0;
        return (int)(($date->diff($this->dateRef)->format('%a'))*$this->coeff/7);
    }

    private function wDiff($date1, $date2)
    {
        if($date1 < $this->dateRef) $date1 = $this->dateRef;
        if($date2 > $this->dateRefEnd) $date2 = $this->dateRefEnd;
        return (int)(($date2->diff($date1)->format('%a') + 1)*$this->coeff/7);
    }

    private function wDate($index, $invert=false)
    {
        $format = $invert ? 'd/m/Y' : 'Y-m-d';
		$date = clone $this->dateRef;
        return $date->modify('+'.(7*$index).' Days')->format($format);
    }

    public function getName()
    {
        return 'pan_extension';
    }
}
