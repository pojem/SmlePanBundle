<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PanierOrderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //print_r($options['data']->getDate()->format('Y-m-d'));
        $builder
            ->add('panier', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\Panier',
                'property' => 'name'))
            ->add('panierAdherents', 'collection', array(
                'type' => new PanierAdherentType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => false
                ))
            ->add('date', 'date', array('widget' => 'single_text'))
            ->add('productUnits', 'collection', array(
                'type' => new PanierOrderProductUnitType(),
                'allow_add' => true,
                'allow_delete' => true,
                'by_reference' => true
                ))
            /*
            */
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\PanierOrder'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_panierordertype';
    }
}
