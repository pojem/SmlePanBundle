<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PanierAdherentWithType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('amapAdherent', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\AmapAdherent',
                'property' => 'id'
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\PanierAdherent'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_panieradherentwithtype';
    }
}
