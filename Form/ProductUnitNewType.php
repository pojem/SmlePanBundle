<?php

namespace Smle\PanBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ProductUnitNewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('weight')
            ->add('unit', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\Unit',
                'property' => 'label'
                ))
            ->add('product', 'entity', array(
                'class' => 'Smle\PanBundle\Entity\Product',
                'property' => 'id'
                ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Smle\PanBundle\Entity\ProductUnit'
        ));
    }

    public function getName()
    {
        return 'smle_panbundle_productunitnewtype';
    }
}
